from django.conf.urls import url
from .views import homepage
from .views import about
from .views import experiences
from .views import cv
from .views import contacts
from .views import guestbook
from .views import gallery
from .views import movies

#url untuk app kita

urlpatterns=[
    url(r'^homepage', homepage, name='homepage'),
    url(r'^about', about, name='about'),
    url(r'^experiences', experiences, name='experiences'),
    url(r'^cv', cv, name='cv'),
    url(r'^contacts', contacts, name='contacts'),
    url(r'^guestbook', guestbook, name='guestbook'),
    url(r'^gallery', gallery, name='gallery'),
    url(r'^movies', movies, name='movies'),    
]
