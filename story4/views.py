from django.shortcuts import render

# Create your views here.

def homepage(request):
    return render(request, 'homepage.html')

def about(request):
    return render(request, 'about.html')

def experiences(request):
    return render(request, 'experiences.html')

def cv(request):
    return render(request, 'cv.html')

def contacts(request):
    return render(request, 'contacts.html')

def guestbook(request):
    return render(request, 'guestbook.html')

def gallery(request):
    return render(request, 'gallery.html')

def movies(request):
    return render(request, 'movies.html')
